//
//  JPMCNewYorkSchoolsTests.swift
//  JPMCNewYorkSchoolsTests
//
//  Created by Omar Gudino on 8/8/23.
//

import XCTest
@testable import JPMCNewYorkSchools

class JPMCNewYorkSchoolsTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        // Any test you write for XCTest can be annotated as throws and async.
        // Mark your test throws to produce an unexpected failure when your test encounters an uncaught error.
        // Mark your test async to allow awaiting for asynchronous code to complete. Check the results with assertions afterwards.
    }
    
    //Need more experience on writing test for network calls, I will start reading on the subject but I'm sure that with a day or two, I will be able to understand much more about the subject and maybe write my network calls better and/or write tests the appropiate way
    func testGetSchools() {
        XCTAssertNoThrow(NetworkManager.getSchools(NetworkManager.shared), "Success")
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
