//
//  JPMCNetworkManager.swift
//  JPMCNewYorkSchools
//
//  Created by Omar Gudino on 8/8/23.
//

import Foundation

final class NetworkManager {
    static let shared = NetworkManager()
    static let getSchoolsUrlString = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
    static let getSATScoresUrlString = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json?dbn="
    
    //The webservice throws all the available schools at once and I think the API has a way to filter the list, so with more time I would have loved to explore the scenario where we only retrieve the first 40, then when reaching the bottom-half of the list, we get the next 40, etc etc.
    func getSchools(completed: @escaping (Result<[JPMCSchool]?, ErrorEnum>) -> Void) {
        let baseURL = NetworkManager.getSchoolsUrlString
        guard let url = URL(string: baseURL) else {
            completed(.failure(.invalidURL))
            return
        }
        
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let _ =  error {
                completed(.failure(.unableToComplete))
                return
            }
                        
            guard let response = response as? HTTPURLResponse, response.statusCode == 200 else {
                completed(.failure(.invalidResponse))
                return
            }
            
            guard let data = data else {
                completed(.failure(.invalidData))
                return
            }
            
            do {
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                let decodedResponse = try decoder.decode([JPMCSchool].self, from: data)
                completed(.success(decodedResponse))
            } catch {
                completed(.failure(.invalidData))
            }
        }
        task.resume()
    }
    
    func getSATScores(school: JPMCSchool, completed: @escaping (Result<JPMCSATScores?, ErrorEnum>) -> Void) {
        let baseURL = NetworkManager.getSATScoresUrlString + school.dbn
        
        guard let url = URL(string: baseURL) else {
            completed(.failure(.invalidURL))
            return
        }
        
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let _ =  error {
                completed(.failure(.unableToComplete))
                return
            }
                        
            guard let response = response as? HTTPURLResponse, response.statusCode == 200 else {
                completed(.failure(.invalidResponse))
                return
            }
            
            guard let data = data else {
                completed(.failure(.invalidData))
                return
            }
            
            do {
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                let decodedResponse = try decoder.decode([JPMCSATScores].self, from: data)
                completed(.success(decodedResponse.first))
            } catch {
                completed(.failure(.invalidData))
            }
        }
        task.resume()
    }
}
