//
//  ContentView.swift
//  JPMCNewYorkSchools
//
//  Created by Omar Gudino on 8/8/23.
//

import SwiftUI

struct JPMCSchoolsListView: View {
    @StateObject private var viewModel = JPMCSchoolsListViewModel()
    //Tried to keep a naving convention for all files/classes since it was something common in my past position, so just wanted to keep that in mind, but for this exercise I could have just keep the namings simple.
    
    //Not much to say here other than trying to make a simple tableview which loads all the cells, more details about loading all the school in the Network file.
    var body: some View {
        ZStack {
            NavigationView {
                List {
                    ForEach(viewModel.schoolsArray, id: \.dbn) { school in
                        NavigationLink(destination: JPMCSchoolDetailsView(school: school)) {
                            JPMCSchoolCell(school: school)
                        }
                        .navigationTitle(viewModel.navigationTitleString)
                    }
                }
            }
            .onAppear { viewModel.getSchools() }
            
            if viewModel.isLoading { LoadingView() }
        }
        
        .alert(item: $viewModel.alertItem) { AlertItem in
            Alert(title: AlertItem.title, message: AlertItem.message, dismissButton: AlertItem.dismissButton)
        }
    }
}

struct JPMCSchoolsListView_Previews: PreviewProvider {
    static var previews: some View {
        JPMCSchoolsListView()
    }
}
