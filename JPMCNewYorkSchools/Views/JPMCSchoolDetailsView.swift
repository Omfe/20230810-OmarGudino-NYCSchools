//
//  JPMCSchoolDetailsView.swift
//  JPMCNewYorkSchools
//
//  Created by Omar Gudino on 8/8/23.
//

import SwiftUI

struct JPMCSchoolDetailsView: View {
    @StateObject private var viewModel = JPMCSchoolDetailsViewModel()
    let school: JPMCSchool
    
    var body: some View {
        //Not sure how I feel about these constants in here, I would typically adjust to the convention the team has, or if I had to choose, maybe have them in the viewmodel
        let fullAddres = school.primaryAddressLine1 + " " + school.city + ", " + school.zip
        let satMathScore = viewModel.satScores.satMathAvgScore
        let satReadingScore = viewModel.satScores.satCriticalReadingAvgScore
        let satWritingScore = viewModel.satScores.satWritingAvgScore
        
        //I only made a custom UI for the regular text views, since they were the only ones duplicating code, I think I would like to work with a View full of seperate UI components, but again, depends on what the team likes to use.
        List {
            VStack {
                Text(school.schoolName)
                    .font(.system(size: 30))
                    .fontWeight(.bold)
                    .frame(alignment: .top)
                    .multilineTextAlignment(.center)
                    .minimumScaleFactor(0.7)
                    .lineLimit(3)
                    .fixedSize(horizontal: false, vertical: true)
                    .padding()
                JPMCRegularTextCell(text: fullAddres)
                JPMCRegularTextCell(text: school.website)
                JPMCRegularTextCell(text: school.phoneNumber)
                JPMCRegularTextCell(text: school.schoolEmail ?? "")
                JPMCRegularTextCell(text: "SAT Avg Math Score:\n" + satMathScore)
                JPMCRegularTextCell(text: "SAT Avg Reading Score: \n" + satReadingScore)
                JPMCRegularTextCell(text: "SAT Avg Writing Score: \n" + satWritingScore)
                
                Text(school.overviewParagraph)
                    .padding()
                    .frame(alignment: .bottom)
                    .fixedSize(horizontal: false, vertical: true)
            }
        }
        .frame(maxHeight: .infinity, alignment: .top)
        .onAppear { viewModel.getSATScoreForSchool(school: school) }
        .alert(item: $viewModel.alertItem) { AlertItem in
            Alert(title: AlertItem.title, message: AlertItem.message, dismissButton: AlertItem.dismissButton)
        }
        //Would love to explore new options to the case where a view can load 90% of information without a service, but if the other information comes in succesfully, display it. If not, dont hold up the user to see the already downloaded text.
        if viewModel.isLoading { LoadingView() }
        
    }
}

struct JPMCSchoolDetailsView_Previews: PreviewProvider {
    static var previews: some View {
        JPMCSchoolDetailsView(school: JPMCSchool(city: "Manhattan", dbn: "", overviewParagraph: "Overview Text", phoneNumber: "", primaryAddressLine1: "", schoolEmail: "theEmail@gmail.com", schoolName: "Test School of Arts", website: "thewebsite.com", zip: "01010"))
    }
}
