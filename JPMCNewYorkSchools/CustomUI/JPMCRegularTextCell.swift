//
//  JPMCRegularTextCell.swift
//  JPMCNewYorkSchools
//
//  Created by Omar Gudino on 8/10/23.
//

import SwiftUI

struct JPMCRegularTextCell: View {
    let text: String
    
    var body: some View {
        Text(text)
            .multilineTextAlignment(.center)
            .fixedSize(horizontal: false, vertical: true)
    }
}

struct JPMCRegularTextCell_Previews: PreviewProvider {
    static var previews: some View {
        JPMCRegularTextCell(text: "Testing Text")
    }
}
