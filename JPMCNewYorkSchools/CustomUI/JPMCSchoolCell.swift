//
//  SchoolCell.swift
//  JPMCNewYorkSchools
//
//  Created by Omar Gudino on 8/9/23.
//

import SwiftUI

struct JPMCSchoolCell: View {
    var school: JPMCSchool
    
    var body: some View {
        VStack {
            Text(school.schoolName)
                .fontWeight(.semibold)
                .lineLimit(2)
                .minimumScaleFactor(0.7)
                .frame(maxWidth: .infinity, alignment: .leading)
            Text("City: " + school.city)
                .fontWeight(.light)
                .frame(maxWidth: .infinity, alignment: .leading)
        }
    }
}

struct SchoolCell_Previews: PreviewProvider {
    static var previews: some View {
        JPMCSchoolCell(school: JPMCSchool(city:"Manhattan", dbn: "", overviewParagraph: "", phoneNumber: "", primaryAddressLine1: "", schoolEmail: "", schoolName: "School Name", website: "", zip: ""))
    }
}
