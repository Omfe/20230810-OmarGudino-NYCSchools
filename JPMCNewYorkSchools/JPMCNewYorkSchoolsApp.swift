//
//  JPMCNewYorkSchoolsApp.swift
//  JPMCNewYorkSchools
//
//  Created by Omar Gudino on 8/8/23.
//

import SwiftUI

@main
struct JPMCNewYorkSchoolsApp: App {
    var body: some Scene {
        WindowGroup {
            JPMCSchoolsListView()
        }
    }
}
