//
//  JPMCSchoolListViewModel.swift
//  JPMCNewYorkSchools
//
//  Created by Omar Gudino on 8/8/23.
//

import Foundation

final class JPMCSchoolsListViewModel: ObservableObject {
    @Published var alertItem: AlertItem?
    @Published var isLoading = false
    @Published var schoolsArray: [JPMCSchool] = []
    
    let navigationTitleString = "New York Schools"
    
    func getSchools() {
        isLoading = true
        
        NetworkManager.shared.getSchools { [unowned self] result in
            DispatchQueue.main.async {
                isLoading = false
                
                switch result {
                case .success(let schoolsArray):
                    self.schoolsArray += schoolsArray ?? []
                    
                case .failure(let error):
                    switch error {
                    case .invalidData:
                        alertItem = AlertContext.invalidData
                        
                    case .invalidURL:
                        alertItem = AlertContext.invalidURL
                        
                    case .invalidResponse:
                        alertItem = AlertContext.invalidResponse
                        
                    case .unableToComplete:
                        alertItem = AlertContext.unableToComplete
                    }
                }
            }
        }
    }
}
