//
//  JPMCSchoolDetailsViewModel.swift
//  JPMCNewYorkSchools
//
//  Created by Omar Gudino on 8/8/23.
//

import Foundation

final class JPMCSchoolDetailsViewModel: ObservableObject {
    @Published var alertItem: AlertItem?
    @Published var isLoading = false
    @Published var satScores: JPMCSATScores = JPMCSATScores(satCriticalReadingAvgScore: "", satMathAvgScore: "", satWritingAvgScore: "")
    
    func getSATScoreForSchool(school: JPMCSchool) {
        isLoading = true
        
        //Created a default instance for the case where the result is empty(some schools returned empty) or there was a failure
        NetworkManager.shared.getSATScores(school: school) { [unowned self] result in
            DispatchQueue.main.async {
                isLoading = false
                let defaultSatScores = JPMCSATScores(satCriticalReadingAvgScore: "No info", satMathAvgScore: "No info", satWritingAvgScore: "No info")
                
                switch result {
                case .success(let satScores):
                    self.satScores = satScores ?? defaultSatScores
                    
                case .failure(let error):
                    self.satScores = defaultSatScores
                    switch error {
                    case .invalidData:
                        alertItem = AlertContext.invalidData
                        
                    case .invalidURL:
                        alertItem = AlertContext.invalidURL
                        
                    case .invalidResponse:
                        alertItem = AlertContext.invalidResponse
                        
                    case .unableToComplete:
                        alertItem = AlertContext.unableToComplete
                    }
                }
            }
        }
    }
}
