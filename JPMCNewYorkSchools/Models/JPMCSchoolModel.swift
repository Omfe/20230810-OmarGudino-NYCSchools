//
//  JPMCSchoolModel.swift
//  JPMCNewYorkSchools
//
//  Created by Omar Gudino on 8/8/23.
//

import Foundation

struct JPMCSchool: Decodable {
    let city: String
    let dbn: String
    let overviewParagraph: String
    let phoneNumber: String
    let primaryAddressLine1: String
    let schoolEmail: String?
    let schoolName: String
    let website: String
    let zip: String
}

//I did not create a new file for this model since I feel that they are related enough for any developer to find more convenient the 2 models in this one file related to "School" rather than have to switch between files to look at these models
struct JPMCSATScores: Decodable {
    let satCriticalReadingAvgScore: String
    let satMathAvgScore: String
    let satWritingAvgScore: String
}
