//
//  ErrorEnum.swift
//  JPMCNewYorkSchools
//
//  Created by Omar Gudino on 8/9/23.
//

import Foundation

enum ErrorEnum: Error {
    case invalidData
    case invalidResponse
    case invalidURL
    case unableToComplete
}
